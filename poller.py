import select
import socket
import sys
import os
import time
import fcntl

class Poller:
    """ Polling server """
    def __init__(self,port,config):
        self.host = ""
        self.port = port
        self.open_socket()
        self.clients = {}
        self.config = config
        self.cache = {}
        self.size = 1024
        self.debug = False
        self.info = False
        self.request_end = "\r\n\r\n"
        self.GET_REQUEST = "GET"
        self.HOST_HEADER = "Host:"
        self.HTTP_REQUEST_TYPES = ["GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "OPTIONS", "CONNECT", "PATCH"]
        self.last_request = {}
        self.drop_list = []

    def open_socket(self):
        """ Setup the socket for incoming clients """
        try:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
            self.server.bind((self.host,self.port))
            fcntl.fcntl(self.server, fcntl.F_SETFL, os.O_NONBLOCK)
            self.server.listen(5)
        except socket.error, (value,message):
            if self.server:
                self.server.close()
            print "Could not open socket: " + message
            sys.exit(1)

    def run(self):
        """ Use poll() to handle each incoming client."""
        self.poller = select.epoll()
        self.pollmask = select.EPOLLIN | select.EPOLLHUP | select.EPOLLERR
        self.poller.register(self.server,self.pollmask)
        try:
            param_timeout = int(self.config['params']['timeout'])
        except:
            param_timeout = 1

        # Set the Timeout value, t
        t = 0
        while True:
            # poll sockets
            try:
                fds = self.poller.poll(timeout=param_timeout)
            except:
                return
            for (fd,event) in fds:
                # handle errors
                if event & (select.POLLHUP | select.POLLERR):
                    self.handleError(fd)
                    continue
                # handle the server socket
                if fd == self.server.fileno():
                    self.handleServer()
                    continue
                # handle client socket
                self.last_request[fd] = time.time()
                result = self.handleClient(fd)
            if t == 2:
                # Do something about the timeout!
                t = 0
                now = time.time()
                for fd,client in self.clients.iteritems():
                    if now - self.last_request[fd] > param_timeout:
                        self.drop_client(fd)
                for fd in self.drop_list:
                    del self.clients[fd]
                self.drop_list = []
            # Increment the Timeout value, t
            t += 1

    def drop_client(self,fd):
        self.poller.unregister(fd)
        self.clients[fd].close()
        self.drop_list.append(fd)

    def handleError(self,fd):
        self.poller.unregister(fd)
        if fd == self.server.fileno():
            # recreate server socket
            self.server.close()
            self.open_socket()
            self.poller.register(self.server,self.pollmask)
        else:
            # close the socket
            self.clients[fd].close()
            del self.clients[fd]

    def handleServer(self):
        (client,address) = self.server.accept()
        self.clients[client.fileno()] = client
        self.last_request[client.fileno()] = time.time()
        self.poller.register(client.fileno(),self.pollmask)

    def handleClient(self,fd):
        try:
            data = self.clients[fd].recv(self.size)
        except socket.error, e:
            err = e.args[0]
            if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                # TODO: QUESTION: what does the 'algorithm' mean by 'break from loop'
                return
            else:
                sys.exit(1)

        if data:
            self.cache[fd] = self.cache.get(fd, "") + data
            if self.request_end in self.cache.get(fd, ""):
                self.handleRequest(fd)
        else:
            self.poller.unregister(fd)
            self.clients[fd].close()
            del self.clients[fd]

    # Handle a request from the user
    def handleRequest(self,fd):
        if (self.info):
            print "INFO: Request recognized"
            print self.cache.get(fd,"ERROR: Bad Request")

        requests = self.cache[fd].split(self.request_end)
        for request in requests[:-1]:
            response = "HTTP/1.1 "
            response_body = None
            now = self.get_time(time.time())
            server = "BillsServe"
            response_headers = {"Date": now, "Server": server, "Last-Modified": now, "Content-Length": 0, "Content-Type": "text/plain"}
            request_host = None
            headers = request.split('\n')
            request_line = headers[0].split(" ")
            request_type = request_line[0]

            # Check the request, if it is bad return 400 error, else process request
            if (request_type == self.GET_REQUEST) and len(request_line) == 3:
                host = None
                for line in headers[1:]:
                    if self.HOST_HEADER == line[:5]:
                        host = line[6:-1].split(":")[0]
                        break

                # Determine the host that is being requested
                request_host = self.config["hosts"].get(host,self.config["hosts"].get("default"))
                resource = request_line[1] if request_line[1] != "/" else "/index.html"

                # Try to retrieve the resource
                # Setup the headers for that resource
                # Prep the response
                try:
                    with open(request_host + resource) as f:
                        response_body = f.read()
                        response += "200 OK"
                    response_headers["Last-Modified"] = self.get_time(os.stat(request_host+resource).st_mtime)
                    response_headers["Content-Length"] = os.path.getsize(request_host+resource)
                    if (self.debug):
                        print "DEBUG: Content-Length: %s" % (os.path.getsize(request_host+resource))
                    response_headers["Content-Type"] = self.config["media"].get(os.path.splitext(resource)[1][1:],"text/plain")
                except IOError as(errno, strerror):
                    if errno == 13:
                        response += "403 Forbidden"
                    elif errno == 2:
                        response += "404 Not Found"
                    else:
                        response += "500 Internal Server Error"
            else:
                if len(request) == 0 or request_type not in self.HTTP_REQUEST_TYPES:
                    response += "400 Bad Request"
                else:
                    response += "501 Not Implemented"

            if (response[-2:] != "OK"):
                response_body = "<html><head></head><body>" + response[9:] + "</body></html>"
                response_headers["Content-Length"] = len(response_body)

            # Setup the final response
            response += "\r\n"
            for header,value in response_headers.iteritems():
                response += "%s: %s\r\n" % (header, value)
            if (self.debug):
                print "DEBUG: Response is:\n" + response

            # Add the response body to the final response
            if response_body:
                response += "\r\n" + response_body
            else:
                response += "\r\n"
            if(self.info):
                print "INFO: response is:\n" + response

            # Send the response back to the client
            self.clients[fd].send(response)
        self.cache[fd] = requests[-1]

    def get_time(self,t):
        gmt = time.gmtime(t)
        format = '%a, %d %b %Y %H:%M:%S GMT'
        time_string = time.strftime(format, gmt)
        return time_string