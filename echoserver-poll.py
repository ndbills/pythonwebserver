"""
A TCP echo server that handles multiple clients with polling. Typing
Control-C will quit the server.
"""

import argparse

from poller import Poller

class Main:
    """ Parse command line options and perform the download. """
    def __init__(self):
        self.debug = False
        self.parse_arguments()
        self.read_config()
        

    def parse_arguments(self):
        ''' parse arguments, which include '-p' for port '''
        parser = argparse.ArgumentParser(prog='Echo Server', description='A simple echo server that handles one client at a time', add_help=True)
        parser.add_argument('-p', '--port', type=int, action='store', help='port the server will bind to',default=3000)
        self.args = parser.parse_args()

    def read_config(self):
        # Read in the web.config file
        content = None
        self.config = {'hosts': {}, 'media': {}, 'params': {}}
        with open('web.conf') as f:
            content = f.readlines()
        for lines in content:
            # split each line into a list
            line_parts = lines.split(" ")
            if (len(line_parts) == 3):
                # check the first thing the the list
                # add to the correct part of the dictionary
                if (line_parts[0] == "host"):
                    self.config['hosts'][line_parts[1]] = line_parts[2].rstrip()
                if (line_parts[0] == "media"):
                    self.config['media'][line_parts[1]] = line_parts[2].rstrip()
                if (line_parts[0] == "parameter"):
                    self.config['params'][line_parts[1]] = line_parts[2].rstrip()
        if (self.debug):
            print self.config

    def run(self):
        p = Poller(self.args.port, self.config)
        p.run()

if __name__ == "__main__":
    m = Main()
    m.parse_arguments()
    try:
        m.run()
    except KeyboardInterrupt:
        pass