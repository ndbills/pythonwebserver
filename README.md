# Python Web Server
This is designed to be a simple web server that accepts only get requests. This server is written in Python and uses the system sockets to connect clients to this web server. This web server uses a mark and sweep logic to kill connections that last longer than the establised timeout.

This server implements the HTTP/1.1 GET request from clients, processes the request and sends the appropriate response. The configurations for this web server can be found in web.conf. The default host folder is set to be the 'web' folder within this project (these setting can be changed via the web.conf file).

# Code Overview
The server is run with the the command 'python echoserver-poll.py'. The 'Poller' class (Poller.py) is where the backbone of the server is located. Here the server vaiables are set up (in the constructor) and the different functions are called.

The 'run()' function implements the 'mark and sweep' logic to watch for connections lasting longer than the specified timeout.

The remaining code takes care of handling requests and returning an HTTP error based on the type of exception or error caught in the logic.

This was a fun project and I hope you have fun testing it!